#include <stdio.h>
#include <stdlib.h>

int ret;

insanity(arg)
	char *arg;
{
	fprintf(stderr, "found insanity: %s\n", arg);
	ret++;
}

main(argc, argv)
	int argc;
	char *argv[];
{
	int a, b;

	a = b = 0;
	while (a = b)
		;

	if (sizeof(char) != 1)
		insanity("char");
	if ((char) -1 != -1)
		insanity("unsigned char");
	if (sizeof(short) != 2)
		insanity("short");
	if (sizeof(int) != 4)
		insanity("int");
	if (sizeof(long) != sizeof(void *))
		insanity("long");
	if (sizeof(long long) != 8)
		insanity("long long");

	exit(ret);
}

