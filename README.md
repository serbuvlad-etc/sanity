sanity
======

Check against common compiler insanity such as odd type sizes and
unnecessary warnings. To run the test simply run `cc check.c && ./a.out`
replacing cc with your compiler and adding any flags you may find
necessary.

Feel free to contribure tests to the file.
